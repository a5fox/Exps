﻿namespace Exps.Core.Views
{
    public class ExpenseTypeView
    {
        public int ExpenseTypeId { get; set; }
        public string ExpenseTypeName { get; set; }
    }
}