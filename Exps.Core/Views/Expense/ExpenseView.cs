﻿namespace Exps.Core.Views
{
    public class ExpenseGroupView
    {
        public int ExpenseGroupId { get; set; }
        public int ExpenseGroupLevel { get; set; }
        public string ExpenseGroupName { get; set; }
        public string ExpenseGroupParentName { get; set; }
        public string ExpenseGroupTypeName { get; set; }
    }
}
