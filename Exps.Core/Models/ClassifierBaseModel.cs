﻿namespace Exps.Core.Models
{
    public class ClassifierBaseModel : IdentityBaseModel
    {
        public string Name { get; set; }
    }
}